package com.vladtop.gallery.di

import android.content.Context
import androidx.room.Room
import com.vladtop.gallery.data.room.dao.GalleryDao
import com.vladtop.gallery.data.room.databases.GalleryDatabase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class RoomModule {

    @Provides
    fun provideDatabaseDao(database: GalleryDatabase): GalleryDao = database.galleryDao()

    @Provides
    @Singleton
    fun provideDatabase(@ApplicationContext context: Context): GalleryDatabase =
        Room.databaseBuilder(
            context, GalleryDatabase::class.java,
            GalleryDatabase.DB_NAME
        )
            .fallbackToDestructiveMigration()
            .build()
}