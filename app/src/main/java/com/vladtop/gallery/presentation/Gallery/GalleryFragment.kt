package com.vladtop.gallery.presentation.Gallery

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.vladtop.gallery.data.adapters.GalleryAdapter
import com.vladtop.gallery.domain.Image
import com.vladtop.gallery.R
import com.vladtop.gallery.databinding.FragmentGalleryBinding
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class GalleryFragment : Fragment(), GalleryAdapter.OnItemClickListener {
    private lateinit var binding: FragmentGalleryBinding
    private val galleryViewModel: GalleryViewModel by viewModels()
    private val gallery = mutableListOf<Image>()
    private lateinit var galleryAdapter: GalleryAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        binding = FragmentGalleryBinding.inflate(inflater, container, false)
        observeData()
        provideRecyclerView()
        onClickEvents()
        return binding.root
    }

    private fun onClickEvents() {
        onRefreshedClicked()
    }

    private fun onRefreshedClicked() {
        binding.fab.setOnClickListener {
            setEnabled(false)
            galleryViewModel.deleteAll()
            galleryViewModel.getGallery()
        }
    }

    private fun setEnabled(isEnabled: Boolean) {
        if (!binding.fab.isEnabled)
            showRefreshedMessage()
        binding.fab.isEnabled = isEnabled
    }

    private fun showRefreshedMessage() {
        Toast.makeText(
            requireContext(),
            "Refreshed!",
            Toast.LENGTH_SHORT
        )
            .show()
    }

    private fun provideRecyclerView() {
        val recyclerView = binding.galleryRV
        recyclerView.layoutManager = LinearLayoutManager(requireContext())
        galleryAdapter = GalleryAdapter(this, gallery)
        recyclerView.adapter = galleryAdapter
        galleryViewModel.getGallery()
    }

    private fun observeData() {
        observeSuccessData()
        observeErrorData()
    }

    private fun observeSuccessData() {
        galleryViewModel.gallery.observe(viewLifecycleOwner, ::processSuccessData)
    }


    private fun processSuccessData(list: List<Image>) {
        Log.e("gallery", gallery.size.toString())
        Log.e("list", list.size.toString())
        if (gallery.isNotEmpty()) {
            clearList()
        }
        addAll(list)
        setEnabled(true)
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun addAll(list: List<Image>) {
        gallery.addAll(list)
        galleryAdapter.notifyDataSetChanged()
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun clearList() {
        gallery.clear()
        galleryAdapter.notifyDataSetChanged()
    }

    private fun observeErrorData() {
        galleryViewModel.errorData.observe(viewLifecycleOwner, ::processErrorData)
    }

    private fun processErrorData(error: Boolean) {
        if (error) {
            showErrorMessage()
        }
    }

    private fun showErrorMessage() {
        Toast.makeText(
            requireContext(),
            "Gallery is empty!",
            Toast.LENGTH_SHORT
        )
            .show()
    }

    override fun onItemClick(url: String) {
        val bundle = bundleOf(IMAGE_URL to url)
        navigateToImageFragment(bundle)
    }

    private fun navigateToImageFragment(bundle: Bundle) {
        findNavController().navigate(
            R.id.action_listFragment_to_photoFragment,
            bundle
        )
    }

    companion object {
        const val IMAGE_URL = "image url"
    }

}