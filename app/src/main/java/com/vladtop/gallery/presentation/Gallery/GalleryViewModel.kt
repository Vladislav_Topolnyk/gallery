package com.vladtop.gallery.presentation.Gallery

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.vladtop.gallery.data.repositories.GalleryRepository
import com.vladtop.gallery.domain.Image
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class GalleryViewModel @Inject constructor(
    private val repository: GalleryRepository
) : ViewModel() {
    private val _gallery: MutableLiveData<List<Image>> = MutableLiveData()
    val gallery: LiveData<List<Image>> = _gallery
    private val _errorData: MutableLiveData<Boolean> = MutableLiveData()
    val errorData: LiveData<Boolean> = _errorData

    fun getGallery() = viewModelScope.launch {
        val isSuccessful = repository.putGallery()
        if (isSuccessful) {
            _gallery.value = repository.getGallery()
        } else {
            _errorData.value = true
        }
    }

    fun deleteAll() = viewModelScope.launch {
        repository.deleteAll()
    }
}