package com.vladtop.gallery.presentation.Image

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.Glide
import com.vladtop.gallery.databinding.FragmentImageBinding
import com.vladtop.gallery.presentation.Gallery.GalleryFragment

class ImageFragment : Fragment() {
    private lateinit var binding: FragmentImageBinding
    private var imageUrl: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            val url: String? = it.getString(GalleryFragment.IMAGE_URL)
            if (url != null)
                imageUrl = url
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        binding = FragmentImageBinding.inflate(inflater, container, false)
        onClickEvents()
        loadImage(imageUrl)
        return binding.root
    }

    private fun onClickEvents() {
        onBackClicked()
    }

    private fun onBackClicked() {
        binding.backBtn.setOnClickListener {
            navigateToSearchFragment()
        }
    }


    private fun navigateToSearchFragment() {
        findNavController().popBackStack()
    }

    private fun loadImage(url: String) {
        Glide.with(binding.root)
            .load(url)
            .fitCenter()
            .placeholder(android.R.drawable.ic_menu_upload)
            .error(android.R.drawable.stat_notify_error)
            .onlyRetrieveFromCache(true)
            .into(binding.imageView)
    }

}