package com.vladtop.gallery.domain

import com.vladtop.gallery.data.room.entities.GalleryEntity

data class Image(
    val id: String,
    val name: String,
    val author: String,
    val url: String
) {
    fun toEntity() = GalleryEntity(
        id = id,
        name = name,
        author = author,
        url = url
    )
}
