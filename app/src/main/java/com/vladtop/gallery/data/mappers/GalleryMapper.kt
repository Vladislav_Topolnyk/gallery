package com.vladtop.gallery.data.mappers

import com.vladtop.gallery.data.api.DTO.ImageDTO
import com.vladtop.gallery.domain.Image
import retrofit2.Response
import javax.inject.Inject

class GalleryMapper @Inject constructor() {
    fun transform(response: Response<List<ImageDTO>>): List<Image>? {
        return if (response.isSuccessful) {
            val imageList = response.toImageList()
            imageList.ifEmpty { null }
        } else null
    }

    private fun Response<List<ImageDTO>>.toImageList(): List<Image> {
        val list = mutableListOf<Image>()
        body()?.map {
            list.add(
                Image(
                    url = it.urls.small,
                    id = it.id,
                    name = it.description ?: "Unknown",
                    author = it.user.name ?: "Unknown"
                )
            )
        }
        return list
    }

}
