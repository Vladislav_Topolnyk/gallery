package com.vladtop.gallery.data.api

import com.vladtop.gallery.data.api.DTO.ImageDTO
import retrofit2.Response
import retrofit2.http.GET

interface GalleryApi {
    companion object {
        const val BASE_URL = "https://api.unsplash.com"
        private const val CLIENT_ID =
            "896d4f52c589547b2134bd75ed48742db637fa51810b49b607e37e46ab2c0043"
    }

    @GET("/photos/?client_id=$CLIENT_ID")
    suspend fun getGallery(): Response<List<ImageDTO>>
}