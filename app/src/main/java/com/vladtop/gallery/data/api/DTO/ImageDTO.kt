package com.vladtop.gallery.data.api.DTO

import com.google.gson.annotations.SerializedName

data class ImageDTO(
    @SerializedName("id") var id: String = "",
    @SerializedName("description") var description: String? = null,
    @SerializedName("user") var user: UserDTO = UserDTO(),
    @SerializedName("urls") var urls: UrlsDTO = UrlsDTO()
)