package com.vladtop.gallery.data.api.DTO

import com.google.gson.annotations.SerializedName

data class UrlsDTO(
    @SerializedName("small") var small: String = ""
)