package com.vladtop.gallery.data.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.vladtop.gallery.databinding.CardViewImageBinding
import com.vladtop.gallery.domain.Image

class GalleryAdapter(
    private val onItemClickListener: OnItemClickListener,
    private val gallery: List<Image>
) : RecyclerView.Adapter<GalleryAdapter.GalleryViewHolder>() {

    interface OnItemClickListener {
        fun onItemClick(url: String)
    }

    inner class GalleryViewHolder(private val binding: CardViewImageBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(image: Image) {
            binding.root.setOnClickListener {
                onItemClickListener.onItemClick(image.url)
            }
            loadImage(image.url)
            setAuthor(image.author)
            setName(image.name)
        }
        private fun setName(name: String){
            binding.nameTV.text = name
        }
        private fun setAuthor(author: String){
            binding.authorTV.text = author
        }
        private fun loadImage(url: String){
            Glide.with(binding.root)
                .load(url)
                .centerCrop()
                .placeholder(android.R.drawable.ic_menu_upload)
                .error(android.R.drawable.stat_notify_error)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(binding.imageView)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GalleryViewHolder {
        val binding = CardViewImageBinding.inflate(
            LayoutInflater.from(parent.context), parent, false
        )
        return GalleryViewHolder(binding)
    }

    override fun onBindViewHolder(holder: GalleryViewHolder, position: Int) {
        holder.bind(gallery[position])
    }

    override fun getItemCount(): Int = gallery.size
}