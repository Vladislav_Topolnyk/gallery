package com.vladtop.gallery.data.room.dao

import androidx.room.*
import com.vladtop.gallery.domain.Image
import com.vladtop.gallery.data.room.entities.GalleryEntity

@Dao
interface GalleryDao {
    @Insert(entity = GalleryEntity::class, onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertAll(list: List<GalleryEntity>)

    @Query("SELECT * FROM ${GalleryEntity.TABLE_NAME}")
    suspend fun getAll(): List<Image>

    @Query("DELETE FROM ${GalleryEntity.TABLE_NAME}")
    suspend fun deleteAll()
}
