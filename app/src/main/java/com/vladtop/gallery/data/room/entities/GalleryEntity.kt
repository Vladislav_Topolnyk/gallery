package com.vladtop.gallery.data.room.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(
    tableName = GalleryEntity.TABLE_NAME
)
data class GalleryEntity(
    @PrimaryKey
    val id: String,
    @ColumnInfo
    val url: String,
    @ColumnInfo
    val name: String,
    @ColumnInfo
    val author: String
) {
    companion object {
        const val TABLE_NAME = "gallery_entity"
    }
}