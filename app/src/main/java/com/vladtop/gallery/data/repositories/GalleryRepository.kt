package com.vladtop.gallery.data.repositories

import com.vladtop.gallery.data.mappers.GalleryMapper
import com.vladtop.gallery.domain.Image
import com.vladtop.gallery.data.api.GalleryApi
import com.vladtop.gallery.data.room.dao.GalleryDao
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

class GalleryRepository @Inject constructor(
    private val galleryApi: GalleryApi,
    private val mapper: GalleryMapper,
    private val galleryDao: GalleryDao
) {

    private suspend fun getGalleryFromApi(): List<Image>? =
        galleryApi.getGallery()
            .let(mapper::transform)

    suspend fun putGallery(): Boolean = withContext(Dispatchers.IO) {
        val gallery: List<Image>? = getGalleryFromApi()
        if (gallery != null) {
            val galleryEntity = toGalleryEntity(gallery)
            galleryDao.insertAll(galleryEntity)
            true
        } else false
    }

    private fun toGalleryEntity(gallery: List<Image>) = gallery.map {
        it.toEntity()
    }

    suspend fun getGallery(): List<Image> = withContext(Dispatchers.IO) {
        galleryDao.getAll()
    }

    suspend fun deleteAll() = withContext(Dispatchers.IO) {
        galleryDao.deleteAll()
    }
}