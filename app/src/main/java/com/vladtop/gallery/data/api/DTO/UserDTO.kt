package com.vladtop.gallery.data.api.DTO

import com.google.gson.annotations.SerializedName

data class UserDTO(
    @SerializedName("name") var name: String? = null
)