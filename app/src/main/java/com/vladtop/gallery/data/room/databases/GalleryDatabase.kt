package com.vladtop.gallery.data.room.databases

import androidx.room.Database
import androidx.room.RoomDatabase
import com.vladtop.gallery.data.room.dao.GalleryDao
import com.vladtop.gallery.data.room.entities.GalleryEntity

@Database(
    entities = [
        GalleryEntity::class
    ],
    version = 1,
)
abstract class GalleryDatabase : RoomDatabase() {
    companion object {
        const val DB_NAME = "gallery.db"
    }

    abstract fun galleryDao(): GalleryDao
}